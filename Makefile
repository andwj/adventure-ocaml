
OCC=ocamlopt.opt

PROGRAM=advent.exe

all: $(PROGRAM)

clean:
	rm -f $(PROGRAM) *.cmx *.cmi *.o

$(PROGRAM): adventure.ml
	$(OCC) $^ -o $@

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
