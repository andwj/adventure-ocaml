(*
 *  A simple adventure game, in OCaml.
 *
 *  by Andrew Apted, 2023.
 *
 *  this code is licensed as CC0 (i.e. public domain)
 *)

(* ---- Input and Output ---- *)

let print = Stdlib.print_endline

let lowercase_string s =
	String.map Char.lowercase_ascii s
;;

let char_space ch =
	let ascii = Char.code ch in
	(ascii <= 32) || (ascii == 127)
;;

let split_words line =
	let seq  = ref [] in
	let word = ref "" in

	let add_char ch =
		word := !word ^ String.make 1 ch
	in

	let add_word () =
		if String.length !word > 0 then begin
			seq  := !word :: !seq;
			word := ""
		end
	in

	let process_char ch =
		if char_space ch then
			add_word ()
		else
			add_char ch
	in

	String.iter process_char line;

	(* ensure we have the last word *)
	add_word ();

	List.rev !seq
;;

(* ---- Room Definitions ---- *)

let _DIRS = [
	("n", "N");  ("north", "N");
	("s", "S");  ("south", "S");
	("e", "E");  ("east",  "E");
	("w", "W");  ("west",  "W");
	("d", "D");  ("down",  "D");
	("u", "U");  ("up",    "U")
]

type _Exit = {
	dir: string;
	dest: string;
mutable
	obstacle: string
}

type _Room = {
	name: string;
	exits: _Exit list;
mutable
	objects: string list;
	description: string list
}

let _ROOMS = [
	{
		name = "Mountain";
		exits = [ { dir="N"; dest="Forest"; obstacle="" } ];
		objects = [];
		description = [
			"You are standing on a large grassy mountain.";
			"To the north you see a thick forest.";
			"Other directions are blocked by steep cliffs."
		];
	};

	{
		name = "Forest";
		exits = [
			{ dir="S"; dest="Mountain"; obstacle="" };
			{ dir="W"; dest="Lake";     obstacle="" };
			{ dir="E"; dest="Outside";  obstacle="CROCODILE" }
		];
		objects = [ "crocodile"; "parrot" ];
		description = [
			"You are in a forest, surrounded by dense trees and shrubs.";
			"A wide path slopes gently upwards to the south, and";
			"narrow paths lead east and west."
		]
	};

	{
		name = "Lake";
		exits = [ { dir="E"; dest="Forest"; obstacle="" } ];
		objects = [ "steak" ];
		description = [
			"You stand on the shore of a beautiful lake, soft sand under";
			"your feet.  The clear water looks warm and inviting."
		]
	};

	{
		name = "Outside";
		exits = [
			{ dir="W"; dest="Forest"; obstacle="" };
			{ dir="E"; dest="Castle"; obstacle="KEY" }
		];
		objects = [];
		description = [
			"The forest is thinning off here.  To the east you can see a";
			"large castle made of dark brown stone.  A narrow path leads";
			"back into the forest to the west."
		]
	};

	{
		name = "Castle";
		exits = [
			{ dir="W"; dest="Outside";  obstacle="" };
			{ dir="S"; dest="Treasury"; obstacle="PASSWORD" }
		];
		objects = [ "guard"; "carrot" ];
		description = [
			"You are standing inside a magnificant, opulent castle.";
			"A staircase leads to the upper levels, but unfortunately";
			"it is currently blocked off by delivery crates.  A large";
			"wooden door leads outside to the west, and a small door";
			"leads south."
		]
	};

	{
		name = "Treasury";
		exits = [ { dir="N"; dest="Castle"; obstacle=""} ];
		objects = [ "treasure" ];
		description = [
			"Wow!  This room is full of valuable treasures.  Gold, jewels,";
			"valuable antiques sit on sturdy shelves against the walls.";
			"However...... perhaps money isn't everything??"
		]
	}
]

let room_find name =
	(* the wanted room must exist! *)
	List.find (fun r -> r.name = name) _ROOMS
;;

let room_free_exit room dir =
	(* the wanted exit must exist! *)
	let exit = List.find (fun e -> e.dir = dir) room.exits in
	exit.obstacle <- ""
;;

let room_has_obj room obj =
	List.exists (fun o -> o = obj) room.objects
;;

let room_add_obj room obj =
	room.objects <- obj :: room.objects
;;

let room_remove_obj room obj =
	room.objects <- List.filter (fun o -> o <> obj) room.objects
;;

let room_describe name =
	let r = room_find name in
	List.iter (fun line -> print line) r.description;
	List.iter (fun obj  -> print ("There is a " ^ obj ^ " here.")) r.objects
;;

(* ---- Player Definitions ---- *)

let player_loc = ref "Mountain"

let player_found_key = ref false

let player_inventory = ref [ "sword" ]

let player_has_obj obj =
	List.exists (fun o -> o = obj) !player_inventory
;;

let player_add_obj obj =
	player_inventory := obj :: !player_inventory
;;

let player_remove_obj obj =
	player_inventory := List.filter (fun o -> o <> obj) !player_inventory
;;

(* ---- Other Game State ---- *)

let game_over = ref false

let welcome_message () =
	print "";
	print "Welcome to a simple adventure game!";
	print ""
;;

let quit_message () =
	print "Goodbye!"
;;

let solved_message () =
	print "You help yourself to the treasure.";
	print "With your good health and new_found wealth,";
	print "you live happily ever after....";
	print "";
	print "Congratulations, you solved the game!"
;;

(* ---- Game Commands ---- *)

let password = "deserttrain"

let cmd_quit args =
	game_over := true
;;

let cmd_help args =
	print "Use text commands to walk around and do things.";
	print "";
	print "Some examples:";
	print "   go north";
	print "   get the rope";
	print "   drop the lantern";
	print "   inventory";
	print "   unlock door";
	print "   kill the serpent";
	print "   quit"
;;

let cmd_inventory args =
	print "You are carrying:";

	match !player_inventory with
	| []  -> print "    nothing"
	| inv -> List.iter (fun obj -> print ("    a " ^ obj)) inv
;;

let cmd_look args =
	print "";
	room_describe !player_loc
;;

let cmd_go args =
	match args with
	| [] -> print "Go where?"
	| where :: _  ->

	match List.assoc_opt where _DIRS with
	| None -> print "I don't understand that direction."
	| Some dir ->

	let room = room_find !player_loc in

	match List.find_opt (fun e -> e.dir = dir) room.exits with
	| None -> print "You cannot go that way."
	| Some exit ->

	match exit.obstacle with
	| "KEY" ->
		print "The castle door is locked!"
	| "CROCODILE" ->
		print "A huge, scary crocodile blocks your path!"
	| "PASSWORD" ->
		print "The guard stops you and says \"Hey, you cannot go in there";
		print "unless you tell me the password!\"."
	| _ ->
		player_loc := exit.dest;
		print "";
		room_describe !player_loc
;;

let cmd_swim args =
	match !player_loc with
	| "Outside" ->
		print "But the moat is full of crocodiles!"
	| "Lake" ->
		if !player_found_key then
			print "You enjoy a nice swim in the lake."
		else begin
			print "You dive into the lake, enjoy paddling around for a while.";
			print "Diving a bit deeper, you discover a rusty old key!";
			player_found_key := true;
			player_add_obj "key"
		end
	| _ ->
		print "There is nowhere to swim here."
;;

let cmd_drop args =
	match args with
	| [] -> print "Drop what?"
	| obj :: _ ->

	let room = room_find !player_loc in

	if not (player_has_obj obj) then
		print ("You are not carrying a " ^ obj ^ ".")
	else begin
		player_remove_obj obj;
		room_add_obj room obj;
		print ("You drop the " ^ obj ^ ".")
	end
;;

let cmd_get args =
	match args with
	| [] -> print "Drop what?"
	| obj :: _ ->

	let room = room_find !player_loc in

	if not (room_has_obj room obj) then
		print ("You do not see any " ^ obj ^ " here.")
	else
		match obj with
		| "crocodile" ->
			print "Are you serious?";
			print "The only thing you would get is eaten!"
		| "parrot" ->
			print "The parrot nimbly evades your grasp."
		| "guard" ->
			print "A momentary blush suggests the guard was flattered."
		| "treasure" ->
			solved_message ();
			game_over := true;
		| _ ->
			room_remove_obj room obj;
			player_add_obj obj;
			print ("You pick up the " ^ obj ^ ".")
;;

let give_object obj whom =
	let room = room_find !player_loc in

	if not (player_has_obj obj) then
		print ("You can't give a " ^ obj ^ ", since you don't have one!")
	else if not (room_has_obj room whom) then
		print ("There is no " ^ whom ^ "here.")
	else begin
		match obj, whom with
		| "carrot", "parrot" ->
			player_remove_obj obj;
			print "The parrot happily starts munching on the carrot.  Every now";
			print ("and then you hear it say \"" ^ password ^ "\" as it nibbles away on");
			print "that orange stick.  I wonder who this parrot belonged to?"
		| "steak", "crocodile" ->
			player_remove_obj obj;
			print "You hurl the steak towards the crocodile, which suddenly";
			print "snaps into action, grabbing the steak in its steely jaws";
			print "and slithering off to devour its meal in private.";
			room_remove_obj room whom;
			room_free_exit room "E"
		| _, "parrot" | _, "crocodile" | _, "guard" ->
			print ("The " ^ whom ^ " is not interested.")
		| _ ->
			print "Don't be ridiculous!"
	end
;;

let cmd_give args =
	match args with
	| obj :: whom :: _ -> give_object obj whom
	| _ -> print "Give what to whom?"

let cmd_feed args =
	match args with
	| what :: whom :: _ -> give_object what whom
	| _ -> print "Feed what to whom?"
;;

let cmd_open args =
	match args with
	| [] -> print "Open what?"
	| obj :: _ ->

	let room = room_find !player_loc in

	if obj <> "door" then
		print "You cannot open that."
	else if !player_loc <> "Outside" then
		print "There is no door here."
	else if not (player_has_obj "key") then
		print "The door is locked!"
	else begin
		print "Carefully you insert the rusty old key in the lock, and turn it.";
		print "Yes!!  The door unlocks!  However the key breaks into several";
		print "pieces and is useless now.";
		player_remove_obj "key";
		room_free_exit room "E"
	end
;;

let cmd_use args =
	match args with
	| [] -> print "Use what?"
	| obj :: _ ->

	if not (player_has_obj obj) then
		print "You cannot use something you don't have."
	else
		match obj with
		| "key" -> cmd_open [ "door" ]
		| "sword" -> print "You practise your parry skills."
		| _ -> print "Its lack of utility leads to futility."
;;

let cmd_attack args =
	match args with
	| [] -> print "Attack what?"
	| target :: _ ->

	match target with
	| "crocodile" ->
		print "The mere thought of wrestling with that savage beast";
		print "paralyses you with fear!"
	| "guard" when (player_has_obj "sword") ->
		print "You and the guard begin a dangerous sword fight!";
		print "But after ten minutes or so, you are both exhausted and";
		print "decide to call it a draw."
	| "guard" ->
		print "You raise your hands to fight, then notice that the guard";
		print "is carrying a sword, so you shadow box for a while instead."
	| _ when (player_has_obj "sword") ->
		print "You swing your sword, but miss!"
	| _ ->
		print "You bruise your hand in the attempt."
;;

let cmd_say args =
	match args with
	| [] -> print "Say what now?"
	| word :: _ ->

	let room = room_find !player_loc in

	if (word <> password) || (!player_loc <> "Castle") then
		print "Nothing happens."
	else begin
		print "The guard says \"Welcome Sire!\" and beckons you to enter";
		print "the treasury.";
		room_free_exit room "S"
	end
;;

let _COMMANDS = [
	("quit",   cmd_quit);
	("inv",    cmd_inventory);
	("help",   cmd_help);
	("look",   cmd_look);
	("go",     cmd_go);
	("swim",   cmd_swim);
	("drop",   cmd_drop);
	("get",    cmd_get);
	("give",   cmd_give);
	("feed",   cmd_feed);
	("open",   cmd_open);
	("use",    cmd_use);
	("attack", cmd_attack);
	("say",    cmd_say)
]

(* ---- Parsing Helpers ---- *)

let _DUMMY_WORDS = [
	"a"; "an"; "the"; "to"; "with"
]

let _ALIASES = [
	("q", "quit");
	("exit", "quit");
	("i", "inv");
	("invent", "inv");
	("inventory", "inv");
	("l", "look");

	("dive", "swim");
	("take", "get");
	("offer", "give");
	("unlock", "open");
	("kill", "attack");
	("hit", "attack");
	("fight", "attack");
	("apply", "use");
	("speak", "say");
	("tell", "say");

	("north", "n");
	("south", "s");
	("east", "e");
	("west", "w");
	("up", "u");
	("down", "d")
]

let valid_word word =
	not (List.exists (fun w -> w = word) _DUMMY_WORDS)
;;

let expand_word word =
	match word with
	| "croc" -> "crocodile"
	| _  -> word
;;

let rec user_command verb args =
	(* replace aliases with the real verb *)
	try
		let verb2 = List.assoc verb _ALIASES in
		user_command verb2 args

	with Not_found -> try
		let f = List.assoc verb _COMMANDS in
		f args

	with Not_found -> try
		(* try a direction command... *)
		let _ = List.assoc verb _DIRS in
		cmd_go [ verb ]

	with Not_found ->
		print ("I don't understand: " ^ verb)
;;

let parse_user_command line =
	let line  = lowercase_string line in
	let words = split_words line in

	if words <> [] then begin
		(* remove useless words like 'a' and 'the' *)
		let words = List.filter valid_word words in

		(* handle common abbreviations *)
		let words = List.map expand_word words in

		begin
			match words with
			| verb :: args -> user_command verb args
			| [] -> print "Huh?"
		end;

		print ""
	end
;;

let read_and_process_command () =
	try
		(* show a prompt *)
		Stdlib.print_string "> ";

		let line = Stdlib.read_line () in
		parse_user_command line

	with End_of_file ->
		(* just quit on EOF -- not much else we can do! *)
		print "";
		game_over := true
;;

(* ---- Main Program ---- *)

let main () =
	welcome_message ();
	room_describe !player_loc;
	print "";
	while not !game_over do
		read_and_process_command ();
	done;
	quit_message ()
;;

main ();
