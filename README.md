
Adventure
=========

This is a simple text adventure game, written in Ocaml.

It was made as an exercise in learning Ocaml, based on the same
adventure game I wrote from scratch (years ago).  The original was
in Rust, but it has been ported to other languages too, including
my own languages Rowan and Yewberry.

The license is CC0 (i.e. public domain).

